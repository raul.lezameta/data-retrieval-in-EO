import pandas as pd
import os
import numpy as np
from numpy import sin, cos, tan, e
import lmfit

locsFull = {
    "ala": "Alabama",
    "gev": "Gevenich",
}


def read_data(folderPath=""):
    locations = ["ala", "gev"]
    dataPath = {
        "ala": os.path.join(folderPath, "Alabama_data.csv"),
        "gev": os.path.join(folderPath, "Gevenich_data.csv"),
    }

    data = {}
    for loc in locations:
        d = pd.read_csv(dataPath[loc]).rename(columns={"Unnamed: 0": "time"})
        d.time = pd.to_datetime(d.time)
        d = d.set_index(d.time).drop(columns="time")
        data[loc] = d

    specPath = {
        "ala": os.path.join(folderPath, "Alabama_sitespecifics.csv"),
        "gev": os.path.join(folderPath, "Gevenich_sitespecifics.csv"),
    }

    specs = {}
    for loc in locations:
        s = pd.read_csv(specPath[loc], ":", index_col=0, header=None).T
        s = s.rename(columns={
            "CCI Land Cover": "LC",
            "KoeppenGeigerClimateClass": "CC",
            "mean LAI": "LAI"}
        )
        specs[loc] = s
        # specs.append(s)

    return data, specs, locations  # Ala, Gev


def σ0_soil(A, B, m_v):
    σ0_soil_lin = 10 ** ((A + B * m_v)/10)  # m2m-2
    return σ0_soil_lin


def water_cloud_model(params, data, θ=np.deg2rad(40), residuals=True):
    A = params["A"].value
    B = params["B"].value
    ω = params["ω"].value

    SSM = data.SSM_IS
    τ = data.TAU

    ref = data.SIG40

    γ2 = e ** ((-2 * τ) / cos(θ))

    vol = 3 * ω * cos(θ) / 4 * (1 - γ2)
    surf = σ0_soil(A, B, SSM) * γ2

    pred = 10 * np.log10(vol + surf)

    res = pred - ref

    if residuals:
        return res
    else:
        return pd.DataFrame([
            pred.rename("pred"),  # predicted dB
            res.rename("res"),  # residuals dB
            (surf/γ2).rename("σ0_soil"),  # σ0_soil dB
            vol.rename("vol"),  # volume linear
            (surf).rename("surf"),  # surf linear
            (vol + surf).rename("total"),  # total linear
        ]).transpose()
        return pred


def predict_optimized():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    data, specs, locations = read_data(folderPath=dir_path)

    params = {"ala": lmfit.Parameters(),
              "gev": lmfit.Parameters()}

    params["ala"].add(name="ω", value=0.06, min=0.04, max=0.1)
    params["ala"].add(name="A", value=-12.5, min=-20, max=-5)
    params["ala"].add(name="B", value=15, min=0, max=30)

    params["gev"].add(name="ω", value=0.08, min=0.07, max=0.12)
    params["gev"].add(name="A", value=-12.5, min=-20, max=-5)
    params["gev"].add(name="B", value=15, min=0, max=30)

    result = {}  # result[loc].params contains optimized paramters
    σ0 = {}  # predictions with original parameters
    σ0_opt = {}  # predictions with optimized papameters
    for loc in locations:
        result[loc] = lmfit.minimize(
            water_cloud_model, params=params[loc], args=(data[loc], np.deg2rad(40), True))
        σ0[loc] = water_cloud_model(
            params=params[loc], data=data[loc], θ=np.deg2rad(40), residuals=False)
        σ0_opt[loc] = water_cloud_model(
            params=result[loc].params, data=data[loc], θ=np.deg2rad(40), residuals=False)
    return data, specs, locations, result, σ0, σ0_opt


if __name__ == "__main__":
    data, specs, locations, result, σ0, σ0_opt = predict_optimized()
