import pandas as pd
import os

locsFull = {
    "ala": "Alabama",
    "gev": "Gevenich",
}


def read_data(folderPath=""):
    locations = ["ala", "gev"]
    dataPath = {
        "ala": os.path.join(folderPath, "Alabama_data.csv"),
        "gev": os.path.join(folderPath, "Gevenich_data.csv"),
    }

    data = {}
    for loc in locations:
        d = pd.read_csv(dataPath[loc]).rename(columns={"Unnamed: 0": "time"})
        d.time = pd.to_datetime(d.time)
        d = d.set_index(d.time).drop(columns="time")
        data[loc] = d

    specPath = {
        "ala": os.path.join(folderPath, "Alabama_sitespecifics.csv"),
        "gev": os.path.join(folderPath, "Gevenich_sitespecifics.csv"),
    }

    specs = {}
    for loc in locations:
        s = pd.read_csv(specPath[loc], ":", index_col=0, header=None).T
        s = s.rename(columns={
            "CCI Land Cover": "LC",
            "KoeppenGeigerClimateClass": "CC",
            "mean LAI": "LAI"}
        )
        specs[loc] = s
        # specs.append(s)

    return data, specs, locations  # Ala, Gev


if __name__ == "__main__":
    data, specs, locations = read_data(folderPath="Ex1")
