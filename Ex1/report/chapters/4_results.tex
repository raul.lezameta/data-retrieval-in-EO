%!TEX root=../report.tex
\section{Results and Discussion}

The two sites of interest can be assumed to be fairly similar due to their Köppen-Geiger climate classification. Both Alabama and Gevenich are classified as \emph{Cf} (temperate, no dry season), while Alabama is \emph{Cfa} (hot summer) and Gevenich is \emph{Cfb} (warm summer). Due to this lack of seasonal change in precipitation, calibration between the two locations should yield reasonable results.


\subsection{Calibration results}
The automatic parameter calibration approach specified in chapter \ref{sec:methods} returned the following parameters:

\begin{table}[H]
    \centering
    \begin{tabular}{c|c|c|c|c}
                     & min & max  & Alabama     & Gevenich    \\ \hline
        $\omega$     & 0   & 0.25 & 0.01968616  & 0.01274179  \\ \hline
        $A$ {[}dB{]} & -20 & -5   & -10.0447419 & -6.16470998 \\ \hline
        $B$ {[}dB{]} & 0   & 25   & 9.45704092  & 2.97602721  \\
    \end{tabular}
    \caption{Ranges and calibration results ($\omega$, $A$, $B$) for Alabama and Gevenich.}
    \label{table:calib}
\end{table}

% raul: was passiert wenn B auf 15 gezwungen wird? least mean square steigt aber macht omega mehr sinn und andere werte?
% citation für konings

The range, in which the parameters are being optimized for was originally taken from the exercise slides provided to us, but had to be altered due to unexpected behavior of B. As can be seen, the resulting values for B are far off the 15 db suggested in the slides, especially for Gevenich. We assume that those heavily distorted values for B then influence the other parameters, specially the albedo $\omega$.

\cite{koningsLbandVegetationOptical2017a} suggests scattering albedo between 0.04 and 0.1 for shrublands and between 0.07 and 0.12 for croplands (values between the 25th and 75th percentile). As shown in Table \ref{table:calib}, neither of the locations fit their respective class. We tried restricting B to a value closer to what we would expect to see what effect it has on the other values. In the case of restricting B to a minimal value of 14 we got following results:

\begin{table}[H]
    \centering
    \begin{tabular}{c|c|c}
                     & Alabama     & Gevenich    \\ \hline
        $\omega$     & 0.05694483  & 0.25000000  \\ \hline
        $A$ {[}dB{]} & -12.3738282 & -10.8359707 \\ \hline
        $B$ {[}dB{]} & 14.0000000  & 14.0000000  \\
    \end{tabular}
    \caption{Calibration results ($\omega$, $A$, $B$) for Alabama and Gevenich with restricted B.}
    \label{table:calib14}
\end{table}

Notably, $\omega$ for Alabama is within the expected range for shrubland while for Gevenich it takes its maximum allowed value outside of any classification, although the effect on Alabama could be coincidental. The mean absolute error for Alabama is almost identical to that of the unrestricted optimization, while it is significantly worse for Gevenich.


\iffalse
    \begin{table}[p]
        \centering
        \begin{tabular}{c|c|c}
                     & min & max  \\ \hline
            $\omega$ & 0   & 0.25 \\ \hline
            A        & -20 & -5   \\ \hline
            B        & 0   & 25
        \end{tabular}
        %\caption{Ranges used for estimation}
    \end{table}
\fi

\subsection{Model performance}
The Mean Absolute Error ($MAE={\frac {1}{n}}\textstyle \sum \mid {\sigma^0_{ref} - \sigma^0_{pred}\mid }$) suggests  the modeling performances to be better in Gevenich than in Alabama:

Mean Absolute Error:
\begin{itemize}
    \item Alabama: 0.64 dB
    \item Gevenich: 0.44 dB
\end{itemize}

The same thing can also be seen in Figure \ref{fig:residuals} where the residuals in Gevenich are clearly smaller than in Alabama. Additionally, this plot also shows the significant performance increases of the optimized parameters compared to the default values.

There seem to be clear seasonal dynamics in the residuals with the not optimized  parameters, specially in Gevenich where there residuals seem to peek in the summer months. A zoomed in version of only the residuals of 2015 can be found in the appendix (Figure \ref{fig:residualsCut})

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\textwidth]{../plots/residuals/ala_absolute.png}
    \includegraphics[width=0.8\textwidth]{../plots/residuals/gev_absolute.png}
    \caption{Absolute residuals of $\sigma^0$ with default and optimized parameters}
    \label{fig:residuals}
\end{figure}

Additionally, the pearson R correlation coefficient was used as an index for the modeling performance. Interestingly Alabama has a higher correlation coefficient of $R = 0.74$ compared to Gevenich ($R = 0.62$) even though the MAE is smaller in Gevenich. The scatter plots of reference vs. predicted $\sigma^0$ (Figure \ref{fig:predVref}) also suggest a better correlation for Alabama.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.49\textwidth]{../plots/regression/regr_ala.png}
    \includegraphics[width=0.49\textwidth]{../plots/regression/regr_gev.png}
    \caption{Scatter plot of reference vs. predicted $\sigma^0$}
    \label{fig:predVref}
\end{figure}

In Figure \ref{fig:sigma0} (and the zoomed-in view: Figure \ref{fig:sigma02015}) in the appendix reference and predicted $\sigma^0$ are overlapped to compare them. It is apparent that while the prediction is able to follow the overall trend of reference $\sigma^0$ it can not catch all the short term oscillations present in measured $\sigma^0$.

\subsection{Long- and short-term dynamics}

As was specified in Equation \ref{eq:waterCloudModel} the total backscatter ($\sigma^0$) is the sum of the backscatter contributions from volume ($\sigma^0_{vol}$) and surface ($\sigma^0_{surf}$) scattering. Figure \ref{fig:backscatterContrib} (and the detail view Figure \ref{fig:backscatterContrib2015}) shows that the total backscatter is clearly dominated by surface (soil) scattering, whereas the volume scattering contributions (by vegetation) are significantly lower. Additionally, there seems to be a strong seasonality in $\sigma^0$, which is mainly driven by the seasonality in $\sigma^0_{surf}$ with decreasing backscatter in the summer months (June to August in Gevenich and December to February in Alabama). The contribution from vegetation ($\sigma^0_{vol}$) is constant for the region of Alabama, while in Gevenich a clear vegetational seasonality is apparent with $\sigma^0_{vol}$ increasing in summer.

