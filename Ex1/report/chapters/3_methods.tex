%!TEX root=../report.tex

\section{Methods}
\label{sec:methods}

\subsection{Water Cloud Model}

The water cloud model or $\tau - \omega$ model models the backscatter of a vegetated land surfaces using the following relation:
\begin{equation}
    \sigma^0 = \sigma^0_{vol} + \sigma^0_{surf} + \sigma^0_{int}
\end{equation}


where ${vol}, {surf}, {int}$ denote the contributions from volume, surface and interaction.

The interaction term describes the contribution from scattering interactions between volume (canopy) and surface (soil). Due to its small contribution it can be neglected.

Without the interaction term the $\tau - \omega$ model can be rewritten in the following form:

\begin{equation}
    \label{eq:waterCloudModel}
    \sigma^0(\theta) =
    \underbrace{\cos(\theta) \frac{3 \omega}{4} \left(1 - \gamma^2(\theta)\right)}_{\sigma^0_{vol}} +
    \underbrace{\sigma^0_{soil}(\theta) \gamma^2(\theta)}_{\sigma^0_{surf}}
\end{equation}

According to Equation \ref{alg:waterCloudModel} the total backscatter depends on:
\begin{itemize}
    \item the incidence angle ($\theta$),
    \item the single-scattering albedo ($\omega$), which describes the ratio of scattering and extinction efficiencies,
    \item vegetation optical depth ($\tau$), describing the vegetation attenuation properties for microwaves
    \item and the backscatter from bare soil ($\sigma^0_{soil}$).
\end{itemize}

$\tau$ is contained in the two-way transmissivity ($\gamma^2$), which can be calculated using the following formula:

\begin{equation}
    \gamma^2(\theta) = e^{-\frac{2 \tau}{\cos(\theta)}}
\end{equation}

$\gamma^2$ controls the relative contribution of the vegetation layer and the soil surface. A very large $\gamma^2$ means that the backscatter is dominated by the soil rather than by the vegetation.

The linear model is used to model the bare soil backscatter coefficient since numerous field experiments demonstrated that $\sigma^0_{soil}$  exhibits a linear relation with soil moisture ($m_v$) when expressed in dB:

\begin{equation}
    \label{eq:linModeldB}
    \sigma^0_{soil} \left[dB\right] = A + B m_v
\end{equation}

$A$ and $B$ are two empirical parameters of the model, depending on sensor and surface properties. The parameter $A$ represents the backscatter coefficient for dry soil while $B$ is the sensitivity to soil moisture.

Since Equation \ref{eq:waterCloudModel} is in the linear domain, Equation \ref{eq:linModeldB} has to converted to the linear domain as well:

\begin{equation}
    \label{eq:linModelLin}
    \sigma^0_{soil} \left[m^2 m^{-2} \right] = 10^{\frac{A + B m_v}{10}}
\end{equation}

In order to compare the modeled backscatter coefficient from Equation \ref{eq:waterCloudModel} with the measured one, it had to be converted to $dB$:

\begin{equation}
    \sigma^0[dB] = 10 \cdot \log_{10}(\sigma^0[m^2 m^{-2}])
\end{equation}


\subsubsection{Implementation}

The Water Cloud Model was implemented in python using two custom defined functions. One for the bare soil backscatter:


\begin{algorithm}[H]
    \begin{lstlisting}[language=Python]
def $\sigma$0_soil(A, B, m_v):
    return 10 ** ((A + B * m_v)/10) #m^2m^-2
    \end{lstlisting}
    \caption{Bare soil backscatter}
    \label{alg:bareSoil}
\end{algorithm}

And one for the water cloud model:

\begin{algorithm}[H]
    \begin{lstlisting}[language=Python]
def water_cloud_model(params, data, $\theta$ = np.deg2rad(40), residuals = True):
    A = params["A"].value
    B = params["B"].value
    $\omega$ = params["$\omega$"].value
    
    SSM = data.SSM_IS
    $\tau$ = data.TAU
    
    ref = data.SIG40
    
    $\gamma$2 = e ** ((-2 * $\tau$) / cos($\theta$))
    
    vol = 3 * $\omega$ * cos($\theta$) / 4 * (1 - $\gamma$2) 
    surf = $\sigma$0_soil(A, B, SSM) * $\gamma$2
    
    pred = 10 * np.log10(vol + surf)
    
    res = pred - ref
    
    if residuals:
        return res
    else:
        return pred
    \end{lstlisting}
    \label{alg:waterCloudModel}
    \caption{Water cloud model implementation in python}
\end{algorithm}

The function takes the parameters $A$, $B$ and $\omega$ together with measured in situ soil moisture and $\tau$, predicts the backscatter coefficient and returns the residuals between the predicted and reference backscatter coefficient ($\sigma^0_{residuals} = \sigma^0_{pred} - \sigma^0_{ref}$).

The parameters are then optimized by trying to minimize the residuals.
%\clearpage

\subsection{Parameter optimization}

The parameters were optimized using the lmfit python package \citep{mattnewvilleLmfitLmfitpy2020}.

It allows the definition of the parameters using a default value as well as a search range providing a min and max value:

\begin{algorithm}[H]
    \begin{lstlisting}[language=Python]
import lmfit

params = lmfit.Parameters()
params.add(name = "A", value = -12.5, min = -20, max = -5)
params.add(name = "B", value = 15, min = 0, max = 20)
params.add(name = "$\omega$", value = 0.125, min = 0, max = 0.25)
    \end{lstlisting}
    \caption{Parameter definition using lmfit}
    \label{alg:paramDef}
\end{algorithm}

The parameters are then optimized by calling the \lstinline[language = Python]|lmfit.minimize()| function on the previously defined \lstinline[language = Python]|water_cloud_model| (algorithm \ref{alg:waterCloudModel}) function:

\begin{algorithm}[H]
    \begin{lstlisting}[language=Python]
result = lmfit.minimize(water_cloud_model, params=params, args=(data, np.deg2rad(40), True), method = "leastsq")
    \end{lstlisting}
    \caption{Parameter optimization by minimizing residuals}
    \label{alg:paramOpt}
\end{algorithm}

The \lstinline[language = Python]|method| argument allows to choose the optimization method. lmfit defaults to \lstinline[language = Python]|"leastsq"| which is an implementation of the Levenberg-Marquardt method.

It works by minimizing the sum of squared residuals between reference and predicted $\sigma^0$:

% \begin{equation}
%     \frac12 \sum^N_{i=1} \left(f \left( \Omega_i,x \right) - y_i\right)^2,x \in {\rm I\!R}^p
% \end{equation}

\begin{equation}
    \frac12 \sum^N_{i=1} \left( \sigma^0_{ref} - \sigma^0_{pred}\right) ^2
\end{equation}

%\clearpage