"""
TASK 2: INVERSE MODELLING
        - model soil moisture from backscatter observations (MetOp ASCAT)
            m_v = (sig0_soil - A) / B
        - validate model output with in situ soil moisture data and a land surface model
        - TU Wien method: back scatter only depends on
                            incidence angle theta,
                            soil moisture (dry and wet) m_s
                            and the vegetational state V
                          direct inversion
                            m_s = (sig0 - sig_dry) / (sig_wet - sig_dry)
                            ...sig_wet, sig_dry: calculated using slope/curvature
                                                    of backscatter-incidence angle
                                                    realationship and backscatter
                                                    (cross-over angles: 25° for dry,
                                                    40° for wet)
        Salem is located in Missouri, middle-west of the US (mostly Grassland)
        Köppen-Geiger Climate Class: Dfa (Hot Summer Continental Climate)
                                        hot summers, snowy winters
                                        e.g. US, Canada, Kazakhstan
"""

import pandas as pd
import numpy as np
from pandas import DataFrame
import os
import statistics
import matplotlib.pyplot as plt

import pytesmo.scaling as scaling
import pytesmo.metrics as metrics


"""
Read data for the station Salem and the site specifications
"""


def read_Data(folderPath=""):
    dataPath = {
        "sal": os.path.join(folderPath, "Salem-10-W_data.csv")
    }

    data = []
    for p in dataPath:
        d = pd.read_csv(dataPath[p]).rename(columns={"Unnamed: 0": "time"})
        d.time = pd.to_datetime(d.time)
        d = d.set_index(d.time).drop(columns="time")
        data.append(d)

    specPath = {"sal": os.path.join(
        folderPath, "Salem-10-W_sitespecifics.csv")}

    specs = []
    for d in specPath:
        s = pd.read_csv(specPath[d], ":", index_col=0, header=None).T
        s = s.rename(columns={
            "CCI Land Cover": "LC",
            "KoeppenGeigerClimateClass": "CC",
            "mean LAI": "LAI"}
        )
        specs.append(s)

    return data, specs


# all data
[sal], [salSpec] = read_Data(folderPath="Ex2")

# date
t = sal.index

# in situ soil surface moisture data (for validation)
# [m^3/m^3]
# already masked
# +: easy to calibrate
# -: point-scale measurements (scaling errors are likely to occur)
ssm_is = sal['SSM_IS']

# GLDAS-Noah land surface model (for validation)
# [m^3/m^3]
# global soil moisture data
# spatial resolution: 28x28km --> very similar to MetOp (+: reduces scaling errors)
# temporal resolution: 3h --> already matched to backscatter
ssm_lsm = sal['SSM_LSM']

# backscatter, acquired by MetOp-A and normalized to 40° (sigma40)
# sig40[dB] (= 10*log sig40[m^2/m2])
# spatial resolution: 25km
# temporal resolution: ~3 days
sig40 = sal['SIG40']

# reference angle theta_ref = 40°
# slope of backscatter - incidence angle relationship
slope = sal['SLOPE']

# reference angle theta_ref = 40°
# curvature of backscatter - incidence angle relationship
curve = sal["CURVE"]

# Create directories
if __name__ == '__main__':
    # get directory containing this python file.
    project_dir = os.path.dirname(os.path.realpath(__file__))
    # output directory
    out_dir = os.path.join(project_dir, 'results')
    # create the out_dir
    try:
        # try to create it
        os.makedirs(out_dir)
    except FileExistsError:
        # if the file already exists, it will throw a "FileExistsError". In that case, do nothing (pass)
        pass

# Create plots to show data
# In-situ SSM
fig, ax = plt.subplots(figsize=(20, 12))
ax.plot(t, ssm_is, color='purple')
ax.set(xlabel='Date', ylabel='SSM m3/m3',
       title='In-Situ Surface Soil Moisture')
plt.savefig(os.path.join(out_dir, 'ssm_is.png'), bbox_inches="tight")
plt.close()
# Modelled SSM
fig, ax = plt.subplots(figsize=(20, 12))
ax.plot(t, ssm_lsm, color='green')
ax.set(xlabel='Date', ylabel='SSM m3/m3',
       title='Modelled Surface Soil Moisture from Landsurface Model')
plt.savefig(os.path.join(out_dir, 'ssm_lsm.png'), bbox_inches="tight")
plt.close()
# Overlay data
fig, ax = plt.subplots(figsize=(20, 12))
ax.plot(t, ssm_is, color='purple', alpha=0.5)
ax.plot(t, ssm_lsm, color='green', alpha=0.5)
ax.set(xlabel='Date', ylabel='SSM m3/m3',
       title='In-Situ vs Landsurface Modell Surface Soil Moisture')
plt.savefig(os.path.join(out_dir, 'ssm_is_lsm.png'), bbox_inches="tight")
plt.close()


"""
1st Task: Implement TU Wien Method
"""

# Incidence angle normalization (only necessary for dry reference)


def norm_inc(inc_ref, sig, inc, slope, curve):
    sig_ref = sig + slope * (inc_ref - inc) + 1/2 * curve * (inc_ref - inc)**2
    return sig_ref


# Reference angles [°]
inc_dry = 25
inc_wet = 40

# Normalize backscatter for wet and dry reference (for wet this is already done)
# [dB]
sig_dry = norm_inc(inc_dry, sig40, inc_wet, slope, curve)
sig_wet = sig40

# Plot normalized backscatter over time
fig, ax = plt.subplots(figsize=(20, 12))
ax.plot(t, sig_dry, color='orange', label='sig_dry')
ax.plot(t, sig_wet, color='blue', label='sig_wet')
ax.set(xlabel='Date',
       ylabel='Backscatter [dB]', title='Normalized Backscatter')
ax.legend()
plt.savefig(os.path.join(out_dir, 'backscatter_2540.png'), bbox_inches="tight")
plt.close()

# Determine wet and dry reference by taking the median of the 5% lowest (dry)
# and 5% highest (wet) values of the normalized backscatter (5% as recommended
# in script) [dB]
sig_dry_sorted = sig_dry.sort_values()  # ascending
sig_wet_sorted = sig_wet.sort_values(ascending=False)  # descending

sig_dry5 = np.percentile(sig_dry_sorted, 5)
sig_wet5 = np.percentile(sig_wet_sorted, 5)

sig_dry_ref = np.median(sig_dry5)
sig_wet_ref = np.median(sig_wet5)

# Calculate surface soil moisture using the measured backscatter and the
# calculated dry and wet reference parameters
ssm_ascat_db = (sig40 - sig_dry_ref) / (sig_wet_ref - sig_dry_ref)  # [dB]
ssm_ascat = 10**(ssm_ascat_db/10)  # [m2/m2]

# Plot surface soil moisture over time
fig, ax = plt.subplots(figsize=(20, 12))
ax.plot(t, ssm_ascat, color='blue')
ax.set(xlabel='Date', ylabel='Surface Soil Moisture [m2/m2]',
       title='Estimated Soil Moisture from ASCAT Backscatter')
plt.savefig(os.path.join(out_dir, 'ssm_ascat.png'), bbox_inches="tight")
plt.close()

"""
2nd Task: Validation of the soil moisture estimates - functions provided by Pytesmo
          Pytesmo-Documentation: https://pytesmo.readthedocs.io/en/latest/api/pytesmo.html#module-pytesmo.metrics
"""

# store all the provided soil moisture data together in one array
ssm_is_arr = np.array(ssm_is).transpose()
ssm_lsm_arr = np.array(ssm_lsm).transpose()
ssm_ascat_arr = np.array(ssm_ascat).transpose()

# Rescale the different SM estimates to the in-situ measurements, comparing different scaling methods
# Minimum-maximum fit: scales datasets to same minimum and maximum value
ssm_lsm_minmax = scaling.min_max(ssm_lsm_arr, ssm_is_arr)
ssm_ascat_minmax = scaling.min_max(ssm_ascat_arr, ssm_is_arr)

print(ssm_lsm_minmax)
print(ssm_ascat_minmax)

# Mean-standard deviation fit: same mean and standard deviation
ssm_lsm_msd = scaling.mean_std(ssm_lsm_arr, ssm_is_arr)
ssm_ascat_msd = scaling.mean_std(ssm_ascat_arr, ssm_is_arr)

print(ssm_lsm_msd)
print(ssm_ascat_msd)

# Linear regression: scale one dataset to the other in a linear way
ssm_lsm_linreg = scaling.linreg(ssm_lsm_arr, ssm_is_arr)
ssm_ascat_linreg = scaling.linreg(ssm_ascat_arr, ssm_is_arr)

print(ssm_lsm_linreg)
print(ssm_ascat_linreg)

# CDF matching: match the cumulative distribution function (CDF) of two datasets (not linear)
ssm_lsm_cdf = scaling.cdf_match(ssm_lsm_arr, ssm_is_arr)
ssm_ascat_cdf = scaling.cdf_match(ssm_ascat_arr, ssm_is_arr)

print(ssm_lsm_cdf)
print(ssm_ascat_cdf)

# NOTE: advantage of 'real' data; problem of point-scale measurement (might lead to scaling errors)


# Calculate validation metrics, comparing different quality criteria
# here, CDF matching is the chosen method for the rescaling
ssm_is_scaled = ssm_is_arr
ssm_lsm_scaled = ssm_lsm_cdf
ssm_ascat_scaled = ssm_ascat_cdf

# Pearson correlation coefficient: R = sig_xy / (sig_x*sig_y)
ssm_lsm_pearson = metrics.pearsonr(ssm_lsm_scaled, ssm_is_scaled)
ssm_ascat_pearson = metrics.pearsonr(ssm_ascat_scaled, ssm_is_scaled)

print(ssm_lsm_pearson)
print(ssm_ascat_pearson)

# Spearman correlation coefficient: rho = sig_rank_xy / (sig_rank_x*sig_rank_y)
# equals for linearly related datasets the Pearson Corr. Coeff.
ssm_lsm_spearman = metrics.spearmanr(ssm_lsm_scaled, ssm_is_scaled)
ssm_ascat_spearman = metrics.spearmanr(ssm_ascat_scaled, ssm_is_scaled)

print(ssm_lsm_spearman)
print(ssm_ascat_spearman)

#Bias: mean_x - mean_y
ssm_lsm_bias = metrics.bias(ssm_lsm_scaled, ssm_is_scaled)
ssm_ascat_bias = metrics.bias(ssm_ascat_scaled, ssm_is_scaled)

print(ssm_lsm_bias)
print(ssm_ascat_bias)

# Standard deviation ratio: sig_x / sig_y
stdev_ssm_is = statistics.stdev(ssm_is_scaled)
stdev_ssm_lsm = statistics.stdev(ssm_lsm_scaled)
stdev_ssm_ascat = statistics.stdev(ssm_ascat_scaled)

ssm_lsm_sdratio = stdev_ssm_lsm / stdev_ssm_is
ssm_ascat_sdratio = stdev_ssm_ascat / stdev_ssm_is

print(ssm_lsm_sdratio)
print(ssm_ascat_sdratio)

# Root mean square difference: measure for random errors when datasets are unbiased (after rescaling)
ssm_lsm_rmsd = metrics.rmsd(ssm_lsm_scaled, ssm_is_scaled)
ssm_ascat_rmsd = metrics.rmsd(ssm_ascat_scaled, ssm_is_scaled)

print(ssm_lsm_rmsd)
print(ssm_ascat_rmsd)
